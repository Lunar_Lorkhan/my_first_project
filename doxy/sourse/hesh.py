import sys
import doxypypy


def check_input(n: int) -> int:
    """
    Функция проверки ввода.

    Функция для проверки ввода.Примает на вход число. В случае, если подаваемое значение не является числом, вызывается
    исключение TypeError, если число меньше нуля или нецелое, вызывается иключение ValueError.

    Args:
        n:  Целое число.

    Returns:
        n:  Целое число

    Raises:
        ValueError, TypeError


    Examples:
        >>>check_input(3)
        3
        >>>check_input(-11)
        input correct value.
        >>>check_input('abc')
        input correct value.
        >>>check_input([1,2,3,4])
        input correct value.
    """
    try:
        if not isinstance(n, int):
            raise TypeError
        elif n < 0:
            raise ValueError
        return n
        print(n)
    except (TypeError, ValueError) as e:
        if e.args:
            print(f'input correct value. {e.args[-1]}')
        else:
            print('input correct value.')


def foo(n):
    """
    Функция вычисления мат. функии.

    Функция для мат. функции. Принимает на вход число n. Возращает 'Nope', если число является четным, и значеие x и y,
    если значение N нечетное. При x = 1 функция всегда принимает наименьшее значение.

    Args:
        n:  Целое число.

    Returns:
        Nope or (1, y)


    Examples:
        >>>foo(17)
        (1, 7)
        >>>foo(16)
        'Nope'
        >>>foo('abc')
        Traceback (most recent call last):
        ...
        TypeError: not all arguments converted during string formatting
        >>>foo([1,2,3,4])
        Traceback (most recent call last):
        ...
        TypeError: unsupported operand type(s) for %: 'list' and 'int'
    """
    return 'Nope' if n % 2 != 1 else (1, int((n - 3)/2))


if __name__ == '__main__':
    answer = None
    while answer is None:
        answer = check_input(input())
    print(foo(answer))
