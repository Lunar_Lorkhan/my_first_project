import hesh
import pytest


def test_check_input1():
    """The function for check input"""
    assert hesh.check_input(15) == 15

def test_check_input2():
    """The function for check input"""
    assert hesh.check_input(-16) == None

def test_check_input3():
    """The function for check input"""
    assert hesh.check_input('alalallalal') == None

def test_check_input4():
    """The function for check input"""
    assert hesh.check_input([1, 2, 3]) == None
    assert hesh.check_input({}) == None
    assert hesh.check_input((2, )) == None

def test_foo1():
    """The function for check answer"""
    assert hesh.foo(15) == (1, 6)
    assert hesh.foo(17) == (1, 7)

def test_foo2():
    """The function for check answer"""
    assert hesh.foo(16) == 'Nope'
    assert hesh.foo(18) == 'Nope'